FROM ldr.lifafa.team:5000/node

VOLUME /tmp

RUN mkdir -p /nodejs-jenkins-helloworld
WORKDIR /nodejs-jenkins-helloworld
COPY ./ /nodejs-jenkins-helloworld

RUN npm install

COPY . .

EXPOSE 3000

CMD npm start

#FROM ldr.lifafa.team:5000/openjdk8-alpine
#VOLUME /tmp

#RUN apk --update --no-cache add build-base less git openssh bash curl maven

#RUN mkdir -p /payment-engine-service
#WORKDIR /payment-engine-service
#COPY ./ /payment-engine-service

#EXPOSE 8080

#RUN mvn package -Dmaven.test.skip=true

#CMD ["./bin/docker-entrypoint.sh"]
